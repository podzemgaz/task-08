package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.bean.Student;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private final String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public List<Student> getResult() {
		List<Student> students = new ArrayList<>();
		String currentElement = null;
		Student student;
		List<String> strings = new ArrayList<>();

		XMLInputFactory factory = XMLInputFactory.newInstance();
		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);
		XMLEventReader reader;
		try {
			reader = factory.createXMLEventReader(new StreamSource(xmlFileName));
			student = new Student();
			while (reader.hasNext()) {

				XMLEvent event = reader.nextEvent();
				// skip any empty content
				if (event.isCharacters() && event.asCharacters().isWhiteSpace())
					continue;
				// handler for start tags
				if (event.isStartElement()) {
					StartElement element = event.asStartElement();
					switch (element.getName().getLocalPart()) {
						case "student":
							Attribute login = element.getAttributeByName(new QName("login"));
							Attribute faculty = element.getAttributeByName(new QName("faculty"));
							student.setLogin(login.getValue());
							student.setFaculty(faculty.getValue());
							break;
						case "name":
							event = reader.nextEvent();
							if (event.isCharacters()) {
								student.setName(event.asCharacters().getData());
							}
							break;
						case "city":
							event = reader.nextEvent();
							if (event.isCharacters()) {
								student.setCity(event.asCharacters().getData());
							}
							break;
						case "phone":
							event = reader.nextEvent();
							if (event.isCharacters()) {
								student.setPhone(Integer.parseInt(event.asCharacters().getData()));
							}
							break;
					}

				}
				if (event.isEndElement()) {
					EndElement endElement = event.asEndElement();
					// if </student>
					if (endElement.getName().getLocalPart().equals("student")) {
						students.add(student);
						student = new Student();
					}
				}
			}

			reader.close();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}


		return students;
	}

//	public static void main(String[] args) {
//		STAXController staxController = new STAXController("input.xml");
//		List<Student> list = staxController.getResult();
//		list.forEach(System.out::println);
//	}
}