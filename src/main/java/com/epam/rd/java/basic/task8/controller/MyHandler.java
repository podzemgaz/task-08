package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.bean.Student;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class MyHandler extends DefaultHandler {
    private final StringBuilder currentValue = new StringBuilder();
    List<Student> result;
    Student currentStudent;

    public List<Student> getResult() {
        return result;
    }

    @Override
    public void startDocument() {
        result = new ArrayList<>();
    }

    @Override
    public void startElement(
            String uri,
            String localName,
            String qName,
            Attributes attributes) {

        // reset the tag value
        currentValue.setLength(0);

        // start of loop
        if (qName.equals("student")) {

            // new student
            currentStudent = new Student();

            // staff id
            String login = attributes.getValue("login");
            currentStudent.setLogin(login);

            String faculty = attributes.getValue("faculty");
            currentStudent.setFaculty(faculty);

        }

    }

    public void endElement(String uri,
                           String localName,
                           String qName) {

        if (qName.equals("name")) {
            currentStudent.setName(currentValue.toString());
        }

        if (qName.equals("city")) {
            currentStudent.setCity(currentValue.toString());
        }

        if (qName.equals("phone")) {
            currentStudent.setPhone(Integer.parseInt(currentValue.toString()));
        }

        // end of loop
        if (qName.equalsIgnoreCase("student")) {
            result.add(currentStudent);
        }

    }

    public void characters(char[] ch, int start, int length) {
        currentValue.append(ch, start, length);

    }
}
