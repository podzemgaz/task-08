
package com.epam.rd.java.basic.task8.bean;

import java.math.BigInteger;

public class Student {


    private String name;

    private int phone;

    private String city;

    private String login;

    private String faculty;

    /**
     * Gets the value of the name property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the phone property.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public int getPhone() {
        return phone;
    }

    /**
     * Sets the value of the phone property.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public void setPhone(int value) {
        this.phone = value;
    }

    public String getCity() {
        return city;
    }


    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the login property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets the value of the login property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLogin(String value) {
        this.login = value;
    }

    /**
     * Gets the value of the faculty property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFaculty() {
        return faculty;
    }

    /**
     * Sets the value of the faculty property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFaculty(String value) {
        this.faculty = value;
    }

    @Override
    public String toString() {

        return String.format("name: %s, phone: %010d, city: %s, login: %s, faculty: %s", name, phone, city, login, faculty);
    }
}
