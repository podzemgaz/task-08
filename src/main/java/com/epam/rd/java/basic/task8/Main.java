package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.bean.Student;
import com.epam.rd.java.basic.task8.controller.DOMController;
import com.epam.rd.java.basic.task8.controller.SAXController;
import com.epam.rd.java.basic.task8.controller.STAXController;
import com.epam.rd.java.basic.task8.controller.ValidatorSax;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;

public class Main {
	
	public static void main(String[] args) {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		String schemaName = "input.xsd";
		System.out.println("Input ==> " + xmlFileName);

		ValidatorSax.validate(xmlFileName, schemaName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		List<Student> students = domController.getStudents();
		// sort (case 1)
		// PLACE YOUR CODE HERE
		students.sort(Comparator.comparing(Student::getName));
		
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		createDoc(students, outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		students = saxController.getStudents();
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		students.sort(Comparator.comparing(Student::getCity));
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		createDoc(students, outputXmlFile);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		students = staxController.getResult();
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		students.sort(Comparator.comparing(Student::getFaculty));
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		createDoc(students, outputXmlFile);

	}


	private static void createDoc(List<Student> students, String outputXmlFile) {
		DocumentBuilderFactory documentBuilderFactory =
				DocumentBuilderFactory.newInstance();

		DocumentBuilder documentBuilder = null;
		try {
			documentBuilder =
					documentBuilderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		assert documentBuilder != null;
		Document document = documentBuilder.newDocument();
		String root = "tns:students";
		String namespace = "http://epam.com/rd/java/basic/task8/bean";
		Element rootElement = document.createElementNS(namespace, root);
		document.appendChild(rootElement);

		for (Student s : students) {
			String elementStudent = "student";
			Element emStudent = document.createElement(elementStudent);
			emStudent.setAttribute("faculty", s.getFaculty());
			emStudent.setAttribute("login", s.getLogin());

			String elementName = "name";
			Element emName = document.createElement(elementName);
			String name = s.getName();
			emName.appendChild(document.createTextNode(name));

			String elementPhone = "phone";
			Element emPhone = document.createElement(elementPhone);
			String phone = String.format("%010d", s.getPhone());
			emPhone.appendChild(document.createTextNode(phone));

			String elementCity = "city";
			Element emCity = document.createElement(elementCity);
			String city = s.getCity();
			emCity.appendChild(document.createTextNode(city));

			emStudent.appendChild(emName);
			emStudent.appendChild(emPhone);
			emStudent.appendChild(emCity);

			rootElement.appendChild(emStudent);
		}
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		try {
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(document);
			StreamResult result = new StreamResult(new FileWriter(outputXmlFile));
			transformer.transform(source, result);
		} catch (TransformerException | IOException e) {
			e.printStackTrace();
		}
	}
}
