package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.bean.Student;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private final String xmlFileName;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE
    public List<Student> getStudents() {
        List<Student> students = new ArrayList<>();

        SAXParserFactory spf = SAXParserFactory.newInstance();

        try {
            SAXParser sp = spf.newSAXParser();
            MyHandler handler = new MyHandler();
            sp.parse(xmlFileName, handler);

            students = handler.getResult();

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

        return students;
    }


	/*public static void main(String[] args) {
		SAXController saxController = new SAXController("input.xml");
		List<Student> list = saxController.getStudents();

		list.forEach(System.out::println);
	}*/

}