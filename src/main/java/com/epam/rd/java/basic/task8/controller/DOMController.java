package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.bean.Student;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public List<Student> getStudents() {
		List<Student> students = new ArrayList<>();

		try {
			//creaating DOM-analizator
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			//parsing xml file
			Document document = db.parse(xmlFileName);

			Element root = document.getDocumentElement();
			NodeList studentsNodes = root.getElementsByTagName("student");
			Student student;

			for (int i = 0; i < studentsNodes.getLength(); i++) {
				student = new Student();
				Element studentElement = (Element) studentsNodes.item(i);
				//filling students field
				student.setName(getTextContent(studentElement, "name"));
				student.setCity(getTextContent(studentElement, "city"));
				student.setPhone(Integer.parseInt(getTextContent(studentElement, "phone")));
				student.setFaculty(studentElement.getAttribute("faculty"));
				student.setLogin(studentElement.getAttribute("login"));

				students.add(student);

			}

		} catch (SAXException | ParserConfigurationException | IOException e) {
			e.printStackTrace();
		}

		return students;
	}


	private static String getTextContent(Element el, String elName) {
		NodeList nList = el.getElementsByTagName(elName);
		Node node = nList.item(0);
		return node.getTextContent();
	}

}
