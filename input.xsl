<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tns="http://epam.com/rd/java/basic/task8/bean">
    <xsl:template match="/">
        <html>
            <body>
                <h2>My Students</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th align="left">Name</th>
                        <th align="left">Login</th>
                        <th align="left">Faculty</th>
                        <th align="left">Phone</th>
                        <th align="left">City</th>
                    </tr>
                    <xsl:for-each select="tns:students/student">
                        <tr>
                            <td><xsl:value-of select="name"/></td>
                            <td><xsl:value-of select="@login"/></td>
                            <td><xsl:value-of select="@faculty"/></td>
                            <td><xsl:value-of select="phone"/></td>
                            <td><xsl:value-of select="city"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>